import React, { Component } from 'react';

import classes from './App.module.css';
import GenerateInput from './components/GenerateInput/GenerateInput';
import DisplayInput from './components/DisplayInput/DisplayInput';
import ButtonTitle from './components/ButtonTitle/ButtonTitle';
import DisplayInputs from './components/DisplayInputs/DisplayInputs';

class App extends Component {
  state = {
    show: null,
    inputs: ['none', 'checkbox', 'button', 'text'],
    type: 'none',
    textTitle: '',
    savedInputs: []
  };

  toggleDropdownHandler = () => {
    this.state.show === null
      ? this.setState({ show: 'block' })
      : this.setState({ show: null });
  };

  saveNameHandler = nameOfClicked => {
    this.setState({ type: nameOfClicked.toString() });
    this.toggleDropdownHandler();
  };

  titleChangeHandler = e => {
    this.setState({textTitle: e.currentTarget.value})
  }

  arrayPushHandler = () => {
    if (this.state.type !== 'none') {
    let array = this.state.savedInputs;
    array.push(this.state.type);
    this.setState({savedInputs: array})
  } else {
    console.log('Select Input Type')
  }
  }

  render() {

    return (
      <React.Fragment>
        <div className={classes.MainWrapper}>
          <h1 className={classes.h1ToMainWrapper}>Input generator</h1>
            {/* TEXT WHICH DETERMINES THE NAME OF THE INPUT */}
          <input
            type='text' 
            placeholder='Type your text here, please'
            className={classes.TextToDisplay}
            value={this.state.textTitle}
            onChange={this.titleChangeHandler}
          />

          <div className={classes.DropdownWrapper}>
            <div className='dropdown'>
              {/* BUTTON INPUT TYPES CHANGES NAME WHEN CLICKED DIFFERENT TYPE */}
              <button
                onClick={() => {
                  this.toggleDropdownHandler();
                }}
                className={classes.BtnInputTypes}
              >
                <ButtonTitle title={this.state.type} />
              </button>
              <button className={classes.BtnCreate} onClick={this.arrayPushHandler}>Create</button>
              {/* GENERATES <li> ELEMENTS IN DROPDOWN WINDOW */}
              <div
                className='dropdown-menu'
                style={{ display: this.state.show }}
              >
                <GenerateInput
                  clicked={this.saveNameHandler}
                  inputs={this.state.inputs}
                />
              </div>
            </div>
          </div>
          
          <DisplayInputs savedInputs={this.state.savedInputs}/>
                
          <DisplayInput title={this.state.textTitle} inputType={this.state.type} />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
