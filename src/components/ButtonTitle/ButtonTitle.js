import React from 'react';

import classes from './ButtonTitle.module.css';

const buttonTitle = props => {
    if (props.title === 'none') {
        return <div className={classes.buttonTitle}>Input Types</div>
    } else {
    return (
        <div className={classes.buttonTitle}>{props.title}</div>
    )
}
}

export default buttonTitle;